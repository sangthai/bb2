(function($){
    $(window).on("load",function(){
        $(".my-content").mCustomScrollbar({
            scrollbarPosition: 'outside',
            autoHideScrollbar: false,
            theme:"my-theme"
        });
    });
})(jQuery);